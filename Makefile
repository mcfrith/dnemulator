# ugh, got to keep these up to date:

progs = fasta-bisulf-sim fasta-methyl-sim fasta-paired-chunks	\
fasta-polymorph fasta-random-chunks fastq-sim

all:

prefix = /usr/local
exec_prefix = ${prefix}
bindir = ${exec_prefix}/bin
install:
	mkdir -p ${bindir}
	cp ${progs} ${bindir}

tag:
	git tag -m "" `git rev-list HEAD^ | grep -c .`
