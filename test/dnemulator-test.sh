#! /bin/sh

# Tests for dnemulator

try () {
    echo "# TEST" $@
    $@
    echo
}

cd $(dirname $0)

PATH=..:$PATH

{
    try fasta-paired-chunks -h
    try fasta-paired-chunks -n10 galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -c galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -d galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -cd galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -f500 galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -s0 galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -l20 galGal3-M-32.fa - -
    try fasta-paired-chunks -n10 -l45,50 galGal3-M-32.fa - -
} |
diff -u dnemulator-test.txt -
